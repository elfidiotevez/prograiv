<?php 


  class Salario{



    private $salario;
    private $horasExtra;
    CONST ISSS=0.03;
    CONST AFP=0.0625;

    function __construct($salaraioE, $horaExtraE){
        $this->salario=$salaraioE;
        $this->horasExtra=$horaExtraE;
    }

    public function calcularSalario(){

        $totalHE=$this->horasExtra*10;
        $desISSS= ($this->salario+$totalHE)*self::ISSS;
        $desAFP=($this->salario+$totalHE)*self::AFP;

        return (($this->salario+$totalHE)-$desISSS)-$desAFP;

    }
    public function retornarDatos(){
        echo "Nombre: Juan Perez, su salario total es: {$this->calcularSalario() }";
    }


  }

  $salario = new Salario(5000,1);
  $salario->retornarDatos();













?>